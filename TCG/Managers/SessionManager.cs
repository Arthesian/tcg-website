﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TCG.Entities;

namespace TCG.Managers
{
    public class SessionManager
    {
        // private constructor
        private SessionManager()
        {

        }

        // Gets the current session.
        public static SessionManager Current
        {
            get
            {
                SessionManager session =
                  (SessionManager)HttpContext.Current.Session["__TCG_SESSION__"];
                if (session == null)
                {
                    session = new SessionManager();
                    HttpContext.Current.Session["__TCG_SESSION__"] = session;
                }
                return session;
            }
        }

        // **** add your session properties here, e.g like this:
        public User User { get; set; }
    }
}
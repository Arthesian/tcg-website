﻿using System.Web;
using System.Web.Optimization;

namespace TCG
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/js/libs").Include(
                        "~/Static/__libs/jquery-1.10.2.min.js",
                        "~/Static/__libs/jquery.validate.min.js",
                        "~/Static/__libs/bootstrap.min.js",
                        "~/Static/__libs/modernizr*",
                        "~/Static/__libs/dragula.js",
                        "~/Static/__libs/echarts.common.min.js",
                        "~/Static/__libs/jquery.fancybox.js",
                        "~/Static/__libs/respond.min.js"));

            bundles.Add(new StyleBundle("~/css").Include(
                      "~/Static/__libs/dragula.css",
                      "~/Static/__libs/jquery.fancybox.css",
                      "~/Static/Style/bootstrap.min.css",
                      "~/Static/Style/autocomplete.css",
                      "~/Static/Style/Site.css"));

            // Own written scripts
            bundles.Add(new ScriptBundle("~/js/main").Include(
                "~/Static/Scripts/Modules/user-heartbeat.js",
                "~/Static/Scripts/Modules/auto-complete.js",
                "~/Static/Scripts/Modules/templater.js",
                "~/Static/Scripts/Modules/template-user.js",
                "~/Static/Scripts/Modules/template-deck.js",
                "~/Static/Scripts/Modules/template-medal.js",
                "~/Static/Scripts/Modules/template-card.js",
                "~/Static/Scripts/Modules/html-helper.js",
                "~/Static/Scripts/Modules/charts-helper.js",
                "~/Static/Scripts/Modules/deck.js",
                "~/Static/Scripts/Modules/deck-builder.js",
                "~/Static/Scripts/Modules/deck-analyzer.js"
            ));
        }
    }
}

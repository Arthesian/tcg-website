﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TCG.ViewModels
{
    // TODO ::
    public class UserViewModel
    {
        public Entities.User User { get; set; }

        // Awards, statistics etc.?
        public Dictionary<string, string> Awards { get; set; }
        public Dictionary<string, string> Statistics { get; set; }

        // Top Scores?

        // Achievements?

        // Settings?

        // Friends?
        public List<Entities.User> Friends { get; set; }
    }
}
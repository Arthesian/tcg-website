﻿using System.Threading.Tasks;

namespace TCG.ViewModels
{
    // TODO : This is not used yet
    public class HomeViewModel : BaseViewModel
    {
        // Whatever properties for homepage
        public int UsersOnlineCount;
        public int DecksTotalCount;
        public int CardsTotalCount;

        public HomeViewModel()
        {
        }

        public async Task<int> GetNumberOfDecks()
        {
            // TODO: This will break
            return -1;
        }
    }
}
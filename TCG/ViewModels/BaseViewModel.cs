﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TCG.Entities;
using TCG.Managers;

namespace TCG.ViewModels
{
    public class BaseViewModel
    {
        public User CurrentUser { get { return SessionManager.Current.User; } }
    }
}
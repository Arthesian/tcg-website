﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace TCG.ViewModels
{
    public class RegisterViewModel
    {
        [Required(ErrorMessage = "You will need a username to log in")]
        [MinLength(3)]
        public string Username { get; set; }

        [Required(ErrorMessage = "You probably want to secure your account with a password")]
        [MinLength(8)]
        public string Password { get; set; }

        [Required(ErrorMessage = "Please fill in your password again, to prevent typo's")]
        [Compare("Password")]
        [DisplayName("Repeat Password")]
        public string RepeatPassword { get; set; }

        [Required(ErrorMessage = "Please fill in your e-mail address")]
        [EmailAddress]
        public string Email { get; set; }

        [Required(ErrorMessage = "You will have to accept the terms before you can make use of this platform")]
        public bool AcceptTerms { get; set; }
    }
}
﻿using MongoDB.Driver;
using MongoDB.Driver.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TCG.Entities;
using TCG.Helpers;

namespace TCG.Repositories
{
    public class DeckRepository : BaseRepository
    {
        protected IMongoClient _clientCards;
        protected IMongoDatabase _databaseCards;

        private IMongoCollection<Deck> DeckCollection;

        public DeckRepository()
        {
            _clientCards = new MongoClient("mongodb+srv://arthesian:Zto6uI3nCIagJHlHOLSc@cluster0-hemmp.mongodb.net/admin?retryWrites=true");
            _databaseCards = _clientCards.GetDatabase("Cluster0");

            this.DeckCollection = _databaseCards.GetCollection<Deck>("cards");
        }

        public List<Deck> GetAllDecks()
        {
            var result = DeckCollection.AsQueryable().ToList();

            return result;
        }

        public List<Deck> GetDecksByUser(User user)
        {
            var result = DeckCollection.AsQueryable().ByUser(user);

            return result.ToList();
        }

        public List<Deck> GetDecksByName(string query)
        {
            var result = DeckCollection.AsQueryable().ByName(query);

            return result.ToList();
        }

        public List<Deck> GetDecksByQuery(string query)
        {
            var result = DeckCollection.AsQueryable().ByQuery(query);

            return result.ToList();
        }

        public List<Deck> GetDecksByCard(Card card)
        {
            var result = DeckCollection.AsQueryable().Where(x => x.DeckCardReferences.Values.Any(c => c == card.multiverseid));

            return result.ToList();
        }
    }
}
﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using TCG.Entities;
using TCG.ViewModels;

namespace TCG.Repositories
{
    public class UserRepository : BaseRepository
    {
        protected IMongoClient _clientUsers;
        protected IMongoDatabase _databaseUsers;

        private IMongoCollection<User> UserCollection;

        public UserRepository()
        {
            _clientUsers = new MongoClient("mongodb+srv://arthesian:zA!yeaAlqx5mjC!hgV8r@cluster0-lqjgs.mongodb.net/admin?retryWrites=true");
            _databaseUsers = _clientUsers.GetDatabase("Cluster0");

            this.UserCollection = _databaseUsers.GetCollection<User>("users");
        }

        public List<User> GetAllUsers()
        {
            var result = UserCollection.AsQueryable().ToList();

            return result;
        }

        public List<User> GetAllActiveUsers()
        {
            var result = UserCollection.AsQueryable().Where(x => x.LastHeartBeat > DateTime.UtcNow.AddSeconds(-10)).ToList();

            return result;
        }

        public List<User> GetUsersByQuery(string query)
        {
            var result = UserCollection.AsQueryable().Where(x => x.Email.ToLower().Contains(query) || x.Username.ToLower().Contains(query));

            return result.ToList();
        }

        public User GetUserByUsername(string username)
        {
            if(String.IsNullOrEmpty(username)) { return null; }

            var result = UserCollection.AsQueryable().Where(x => x.Username.ToLower() == username.ToLower()).FirstOrDefault();

            return result;
        }

        public User GetUserByLogin(string username, string passwordHash)
        {
            if(String.IsNullOrEmpty(username) || String.IsNullOrEmpty(passwordHash)) { return null; }

            var result = UserCollection.Find(x => username.Equals(x.Username, StringComparison.InvariantCultureIgnoreCase) && x.PasswordHash == passwordHash).FirstOrDefault();

            return result;
        }

        public async Task<User> GetUserById(string id)
        {
            if(String.IsNullOrEmpty(id)) { return null; }

            var result = await UserCollection.FindAsync(x => x.Id == id);

            return result.FirstOrDefault();
        }

        public void UpdateUser(User user)
        {
            UserCollection.ReplaceOne(x => x.Id == user.Id, user);
        }

        public void RegisterUser(User user)
        {
            UserCollection.InsertOne(user);
        }

        public User ProfileUpdate(User original, User update)
        {
            var user = original;

            user.About = update.About;
            user.ProfileImageUrl = update.ProfileImageUrl;

            return user;
        }
    }
}
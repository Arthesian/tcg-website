﻿using MongoDB.Driver;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TCG.Entities;

namespace TCG.Repositories
{
    public class CardRepository : BaseRepository
    {
        protected IMongoClient _clientCards;
        protected IMongoDatabase _databaseCards;

        private IMongoCollection<Card> CardCollection;

        public CardRepository()
        {
            _clientCards = new MongoClient("mongodb+srv://arthesian:Zto6uI3nCIagJHlHOLSc@cluster0-hemmp.mongodb.net/admin?retryWrites=true");
            _databaseCards = _clientCards.GetDatabase("Cluster0");

            this.CardCollection = _databaseCards.GetCollection<Card>("cards_unique");
        }

        public List<Card> GetAllCards()
        {
            var result = CardCollection.AsQueryable().ToList();

            return result;
        }

        public List<Card> GetCardListByIds(Int64[] multiverseids)
        {
            var result = CardCollection.AsQueryable().Where(x => multiverseids.Contains(x.multiverseid)).ToList();

            return result;
        }

        public List<Card> GetCardsByParameters(dynamic parameters)
        {
            var cards = new List<Card>();

            JObject attributesAsJObject = parameters;
            Dictionary<string, object> values = attributesAsJObject.ToObject<Dictionary<string, object>>();

            var query = CardCollection.AsQueryable();

            if(!String.IsNullOrEmpty(parameters.name))
            {
                var name = (string)parameters.name;
                query.Where(x => x.name.ToLower().Contains(name));
            }

            if (!String.IsNullOrEmpty(parameters.power))
            {
                var power = (string)parameters.power;
                query.Where(x => x.power.Equals(power));
            }

            return query.ToList();
        }

        public List<Card> GetCardsByQuery(string query)
        {
            var result = CardCollection.AsQueryable().Where(x => x.name.ToLower().Contains(query)).ToList();

            return result;
        }

        // Cache outputs for query for 1 day
        [OutputCache(Duration = 86400, VaryByParam = "query")]
        public List<string> GetCardNamesByQuery(string query)
        {
            var result = CardCollection.AsQueryable().Where(x => x.name.ToLower().Contains(query)).Select(x => x.name).Distinct().Take(10).ToList();

            return result;
        }

        public Card GetCardByQuery(string query)
        {
            var result = CardCollection.AsQueryable().Where(x => x.name.ToLower().Contains(query)).Take(24).FirstOrDefault();

            return result;
        }

        public Card GetCardByMultiverseId(int multiverseid)
        {
            var result = CardCollection.AsQueryable().Where(x => x.multiverseid == multiverseid).FirstOrDefault();

            return result;
        }
    }
}
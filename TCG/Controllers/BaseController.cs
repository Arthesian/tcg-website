﻿using System.Web.Mvc;
using TCG.Entities;
using TCG.Managers;
using TCG.Attributes;
using TCG.ViewModels;

namespace TCG.Controllers
{
    [ContextValidation]
    public class BaseController : Controller
    {
        public User CurrentUser { get { return SessionManager.Current.User; } }

        public static BaseViewModel BaseViewModel { get; set; }

        public BaseController()
        {
            BaseViewModel = new BaseViewModel();
        }
    }
}
﻿using System;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using TCG.ViewModels;
using TCG.Attributes;
using Parse;
using TCG.Managers;
using TCG.Entities;
using TCG.Constants;
using TCG.Helpers;
using System.Collections.Generic;
using TCG.Repositories;

namespace TCG.Controllers
{
    public class CardController : BaseController
    {
        private CardRepository Repository;

        public CardController()
        {
            this.Repository = new CardRepository();
        }

        public ActionResult Index()
        {
            return RedirectToAction("Search", "Card");
        }

        [OutputCache(Duration = 3600)]
        public async Task<ActionResult> Search()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> AutoComplete(string name)
        {
            var results = Repository.GetCardNamesByQuery(name);

            return Json(results);
        }

        [HttpPost]
        public async Task<ActionResult> Search(string query)
        {
            var results = Repository.GetCardsByQuery(query);

            return Json(results);
        }

        [Route("Card/Name/{name}")]
        public async Task<ActionResult> Detail(string name)
        {
            return View();
        }

        [Route("Card/Id/{id}")]
        public async Task<ActionResult> Detail(int id)
        {
            return View();
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TCG.Controllers
{
    public class HomeController : BaseController
    {
        /// <summary>
        /// Home Page Method
        /// </summary>
        /// <returns>Home Page</returns>
        /// 
        [OutputCache(Duration = 3600)]
        public ActionResult Index()
        {
            return View(BaseViewModel);
        }
    }
}
﻿using System;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using TCG.ViewModels;
using TCG.Attributes;
using Parse;
using TCG.Managers;
using TCG.Entities;
using TCG.Constants;
using TCG.Helpers;
using System.Collections.Generic;

namespace TCG.Controllers
{
    [Authentication]
    public class ProfileController : BaseController
    {
        public ActionResult Index()
        {
            return RedirectToAction("Login", "Account");
        }

        public async Task<ActionResult> ChangePassword()
        {
            Array results = null; // SearchModel.Results = Array(Cards);

            return View(results);
        }

        [HttpPost]
        public async Task<ActionResult> ChangePassword(string currentPassword, string newPassword)
        {
            Array results = null; // SearchModel.Results = Array(Cards);

            return View(results);
        }

        public async Task<ActionResult> ChangeEmail()
        {
            Array results = null; // SearchModel.Results = Array(Cards);

            return View(results);
        }

        [HttpPost]
        public async Task<ActionResult> ChangeEmail(string currentPassword, string newEmail)
        {
            Array results = null; // SearchModel.Results = Array(Cards);

            return View(results);
        }
    }
}
﻿using System;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using TCG.ViewModels;
using TCG.Attributes;
using TCG.Managers;
using TCG.Entities;
using TCG.Constants;
using TCG.Helpers;
using TCG.Repositories;

namespace TCG.Controllers
{
    public class AccountController : BaseController
    {
        UserRepository UserRepository;

        public AccountController()
        {
            UserRepository = new UserRepository();
        }

        public ActionResult Index()
        {
            return RedirectToAction("Login", "Account");
        }

        [OutputCache(Duration = 3600)]
        public async Task<ActionResult> Login()
        {
            var model = new LoginViewModel();

            if (Request.Cookies[CookieNames.LoginRememberMe] != null && Request.Cookies[CookieNames.LoginRememberMe].Value == bool.TrueString)
            {
                model.Username = Request.Cookies[CookieNames.LoginUsername].Value;
                model.RememberMe = true;

                //return await Login(model);
            }

            // To make sure that if any authorisation attribute data is stored for the post
            TempData.Keep();

            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> Login(LoginViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var userRepository = new UserRepository();

            // Get user by username
            var user = userRepository.GetUserByUsername(model.Username);

            if (user == null)
            {
                ModelState.AddModelError("Username", "No user with this username exists!");
                return View(model);
            }

            // Actual login
            var passwordHash = HashHelper.GetUserHashString(user, model.Password);

            if (passwordHash != user.PasswordHash)
            {
                ModelState.AddModelError("Password", "Password was incorrect!");
                return View(model);
            }

            // Assign User to the current session
            SessionManager.Current.User = user;

            // Remember user
            if (model.RememberMe)
            {
                // Set duration of cookie to 30 days
                var usernameCookie = new HttpCookie(CookieNames.LoginUsername, model.Username);
                usernameCookie.Expires = DateTime.Now.AddDays(30);
                Response.Cookies.Add(usernameCookie);

                var rememberMeCookie = new HttpCookie(CookieNames.LoginRememberMe, bool.TrueString);
                rememberMeCookie.Expires = DateTime.Now.AddDays(30);
                Response.Cookies.Add(rememberMeCookie);
            }else
            {
                // Clear cookies by setting duration until yesterday
                if (Response.Cookies[CookieNames.LoginUsername] != null)
                {
                    Response.Cookies[CookieNames.LoginUsername].Expires = DateTime.Now.AddDays(-1);
                }

                if (Response.Cookies[CookieNames.LoginRememberMe] != null)
                {
                    Response.Cookies[CookieNames.LoginRememberMe].Expires = DateTime.Now.AddDays(-1);
                }
            }

            // Check if the tempdata has values because of authorization attribute
            object controller;
            if (TempData.TryGetValue("controller", out controller))
            {
                string url = "/" + controller.ToString();

                object action;
                if (TempData.TryGetValue("action", out action))
                {
                    string actionName = action.ToString();

                    url += "/" + actionName;
                }

                object parameters;
                if(TempData.TryGetValue("parameters", out parameters))
                {
                    string urlParameters = parameters.ToString();

                    if (!String.IsNullOrEmpty(urlParameters))
                    {
                        url += "/?" + HttpUtility.UrlDecode(urlParameters);
                    }
                }

                // Redirect to original url the user was trying to access
                return Redirect(url);
            }

            // Redirect to profile page after login
            return RedirectToAction("Name","User",new { name = user.Username });
        }

        [OutputCache(Duration = 3600)]
        public async Task<ActionResult> Register()
        {
            var model = new RegisterViewModel();

            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> Register(RegisterViewModel model)
        {
            if (!model.AcceptTerms)
            {
                ModelState.AddModelError("AcceptTerms", "You need to accept the terms to use this platform");
            }

            if (!ModelState.IsValid)
            {
                return View(model);
            }

            // Check if username already exists
            var usernameExists = UserRepository.GetUserByUsername(model.Username);
            if (usernameExists != null)
            {
                ModelState.AddModelError("Username", "Username already exists!");
            }

            if (!ModelState.IsValid)
            {
                return View(model);
            }

            // After validation, create the new user object
            var user = new User()
            {
                Username = model.Username,
                Email = model.Email,
                RegisterDate = DateTime.UtcNow
            };

            // Generate the password hash
            user.PasswordHash = HashHelper.GetUserHashString(user, model.Password);

            // register the user
            try
            {
                UserRepository.RegisterUser(user);
            }
            catch (Exception e)
            {
                ModelState.AddModelError("AcceptTerms", "Something went wrong while creating your account. Please try again");
                return View(model);
            }

            SessionManager.Current.User = user;

            // If successful, redirect to login
            return RedirectToAction("Name", "User", new { name = user.Username });
        }

        public ActionResult Logout()
        {
            SessionManager.Current.User = null;

            // TODO :: Clear login token if it's there

            return RedirectToAction("Index", "Home");
        }

        /**
         * Example methods that DO require authentication, even though the Account Controller doesn't use Authentication
         * 
         */

        [Authentication]
        public ActionResult Update()
        {
            return null;
        }

        [Authentication]
        public ActionResult HandleUpdate()
        {
            return null;
        }
    }
}
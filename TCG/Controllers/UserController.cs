﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Mvc;
using TCG.Repositories;
using TCG.ViewModels;

namespace TCG.Controllers
{
    public class UserController : BaseController
    {
        UserRepository UserRepository;

        public UserController() {
            UserRepository = new UserRepository();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public async Task<ActionResult> Index()
        {
            if(CurrentUser != null)
            {
                return await Name(CurrentUser.Username);
            }else
            {
                return RedirectToAction("Search", "User");
            }
        }

        [OutputCache(Duration = 3600)]
        public async Task<ActionResult> Search()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> Search(string query)
        {
            var results = UserRepository.GetUsersByQuery(query);

            var simpleUsers = new List<object>();

            foreach(var user in results)
            {
                var userObject = new
                {
                    Username = user.Username,
                    ProfileImageUrl = user.ProfileImageUrl,
                    DCINumber = user.DCINumber,
                    Email = user.Email,
                    Culture = user.CultureISOCode,
                    BannerImageUrl = user.BannerImageUrl,
                    TCGLevel = user.TCGLevel,
                    TCGExperiencePoints = user.TCGExperiencePoints,
                    About = user.About,
                    LastActivity = user.LastActivity,
                    LastHeartBeat = user.LastHeartBeat,
                    Status = user.Status
                };

                simpleUsers.Add(userObject);
            }

            return Json(simpleUsers);
        }

        [OutputCache(Duration = 3600, VaryByParam = "name")]
        public async Task<ActionResult> Name(string name)
        {
            var model = new UserViewModel();

            if(string.IsNullOrEmpty(name) && CurrentUser == null) { return View("NoUser"); }

            model.User = CurrentUser?.Username == name ? CurrentUser : UserRepository.GetUserByUsername(name);

            if(model.User == null)
            {
                return View("NoUser");
            }

            return View("Detail", model);
        }

        public async Task<ActionResult> Id(string id)
        {
            var model = new UserViewModel();

            model.User = CurrentUser?.Id == id ? CurrentUser : await UserRepository.GetUserById(id);

            return View("Detail", model);
        }

        [HttpPost]
        public async Task<JsonResult> GetBaseJsonUserById(string id)
        {
            var user = CurrentUser?.Id == id ? CurrentUser : await UserRepository.GetUserById(id);

            JsonSerializerSettings jsSettings = new JsonSerializerSettings();
            jsSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;

            var userObject = new
            {
                Username = user.Username,
                LastActivity = user.LastActivity,
                LastHeartBeat = user.LastHeartBeat,
                Status = user.Status
            };

            return Json(JsonConvert.SerializeObject(userObject, Formatting.None, jsSettings));
        }

        public void HeartBeat(bool? real)
        {
            if (CurrentUser == null) { return; }

            CurrentUser.LastHeartBeat = DateTime.UtcNow;

            if (real.HasValue && real.Value)
            {
                CurrentUser.LastActivity = DateTime.UtcNow;
            }

            UserRepository.UpdateUser(CurrentUser);
        }
    }
}
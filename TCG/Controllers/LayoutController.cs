﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;
using TCG.Helpers;
using TCG.Managers;

namespace TCG.Controllers
{
    public class LayoutController : BaseController
    {
        public ActionResult RenderUserMenu()
        {
            return PartialView("~/Views/Partials/_UserMenu.cshtml", SessionManager.Current.User);
        }

        public ActionResult RandomBackgroundImage()
        {
            var backgroundFolder = new DirectoryInfo(HostingEnvironment.MapPath("~/Static/Images/Backgrounds/"));

            var files = backgroundFolder.GetFiles();

            var rand = new Random();
            var randomFile = files[rand.Next(0, files.Length)];

            FileStream stream = System.IO.File.Open(randomFile.FullName, FileMode.Open);

            return File(stream, "image/jpeg");
        }

        //public async Task<ActionResult> GetCardImage(int multiverseid, string format = "normal")
        //{
        //    var folderPath = "~/Static/Images/Cards/" + format + "/";
        //    var imagePath = "~/Static/Images/Cards/" + format + "/" + multiverseid + ".jpg";

        //    var cardImageFolder = new DirectoryInfo(HostingEnvironment.MapPath(folderPath));

        //    // Download image from scryfall
        //    if(cardImageFolder.GetFiles(multiverseid.ToString()).Length == 0)
        //    {
        //        using (WebClient client = new WebClient())
        //        {
        //            var url = "https://api.scryfall.com/cards/multiverse/" + multiverseid + "?format=image&version=" + format;
        //            client.DownloadFile(new Uri(url), HostingEnvironment.MapPath(imagePath));
        //        }
        //    }

        //    FileStream stream = System.IO.File.Open(HostingEnvironment.MapPath(imagePath), FileMode.Open);

        //    return File(stream, "image/jpeg");
        //}
    }
}
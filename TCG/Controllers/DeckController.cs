﻿using System;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using TCG.ViewModels;
using TCG.Attributes;
using Parse;
using TCG.Managers;
using TCG.Entities;
using TCG.Constants;
using TCG.Helpers;
using System.Collections.Generic;
using TCG.Repositories;

namespace TCG.Controllers
{
    public class DeckController : BaseController
    {
        private DeckRepository Repository;

        public DeckController()
        {
            Repository = new DeckRepository();
        }

        public ActionResult Index()
        {
            if(CurrentUser != null)
            {
                return RedirectToAction("Overview", "Deck");
            }
            else
            {
                return RedirectToAction("Search", "Deck");
            }
        }

        public async Task<ActionResult> Search()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> Search(string query)
        {
            var results = Repository.GetDecksByName(query);

            return View(results);
        }

        [Route("Deck/Name/{name}")]
        public async Task<ActionResult> Detail(string username, string deckname)
        {
            return View();
        }

        [Route("Deck/Id/{id}")]
        public async Task<ActionResult> Detail(int id)
        {
            return View();
        }

        [Authentication]
        public async Task<ActionResult> Overview()
        {
            return View();
        }

        [Authentication]
        public async Task<ActionResult> Create()
        {
            return View();
        }

        [HttpPost]
        [Authentication]
        public async Task<ActionResult> Create(string deckmodel)
        {
            throw new NotImplementedException();
        }

        [Authentication]
        public async Task<ActionResult> Edit()
        {
            throw new NotImplementedException();
        }

        [HttpPost]
        [Authentication]
        public async Task<ActionResult> Edit(string deckmodel)
        {
            throw new NotImplementedException();
        }
    }
}
﻿using Microsoft.Owin;
using Owin;
using Parse;
using TCG.Entities;

[assembly: OwinStartupAttribute(typeof(TCG.Startup))]
namespace TCG
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}

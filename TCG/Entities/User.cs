﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using Parse;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using TCG.Repositories;

namespace TCG.Entities
{
    [BsonIgnoreExtraElements]
    public class User
    {
        // Programmatically assigned values
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        [BsonIgnoreIfDefault]
        public string Id { get; set; }
        public DateTime RegisterDate { get; set; }
        public DateTime LastActivity { get; set; }
        public DateTime LastHeartBeat { get; set; }
        public int TCGExperiencePoints { get; set; }

        // Account login details
        public string Username { get; set; }
        public string PasswordHash { get; set; }

        // Personal information
        public string Email { get; set; }

        private string _ProfileImageUrl;
        public string ProfileImageUrl { get { return String.IsNullOrEmpty(_ProfileImageUrl) ? "/Static/Images/user.png" : _ProfileImageUrl; } set { _ProfileImageUrl = value; } }

        public string BannerImageUrl { get; set; }
        public string CultureISOCode { get; set; }
        public string About { get; set; }

        public Int64 DCINumber { get; set; }
        public string[] FavouriteColors { get; set; }
        public string FavouriteFormat { get; set; }
        public Int64[] FavouriteCardMultiverseIds { get; set; }

        public double TCGLevel
        {
            get
            {
                return Int32.Parse(Math.Round(TCGExperiencePoints - (TCGExperiencePoints * 0.9)).ToString()) + 1; // TODO:: Figure out some fancy exponential formula
            }
        }

        [BsonIgnore]
        public List<Deck> Decks
        {
            get
            {
                // Lazy Loading Decks
                return new DeckRepository().GetDecksByUser(this);
            }
        }

        [BsonIgnore]
        public List<Card> FavouriteCards
        {
            get
            {
                // Lazy Loading Cards
                return new CardRepository().GetCardListByIds(this.FavouriteCardMultiverseIds);
            }
        }

        [BsonIgnore]
        public CultureInfo Culture
        {
            get
            {
                if (string.IsNullOrEmpty(CultureISOCode))
                {
                    CultureISOCode = "en-US";
                }
                return new CultureInfo(CultureISOCode);
            }
            set
            {
                CultureISOCode = Culture.Name;
            }
        }
        [BsonIgnore]
        public string Status
        {
            get
            {
                if (LastHeartBeat.AddSeconds(15) < DateTime.UtcNow) { return "OFFLINE"; }
                if (LastActivity.AddMinutes(1) > DateTime.UtcNow) { return "ONLINE"; }
                else return "INACTIVE";
            }
        }

        public User() { }
    }


}
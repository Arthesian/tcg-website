﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TCG.Entities
{
    [BsonIgnoreExtraElements]
    public class Card
    {
        // Programmatically assigned values
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        [BsonIgnoreIfDefault]
        public string Id { get; set; }
        
        public string[] colors { get; set; }
        public string[] color_identity { get; set; }
        public string[] subtypes;
        public string[] supertypes;
        public string[] types;

        public object[] rulings { get; set; }

        public string name;
        public Int64 multiverseid;
        public decimal cmc;

        public string artist;
        public string layout;
        public string mana_cost;
        public string collector_number;
        public string original_text;
        public string original_type;
        public string power;
        public string rarity;
        public string text;
        public string toughness;
        public string type;

        public string border_color;
        public string set_name;

        public string oracle_text;
        public object image_uris;
    }
}
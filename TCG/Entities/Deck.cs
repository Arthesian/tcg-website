﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TCG.Repositories;

namespace TCG.Entities
{
    [BsonIgnoreExtraElements]
    public class Deck
    {
        // Programmatically assigned values
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        [BsonIgnoreIfDefault]
        public string Id { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime ModifyDate { get; set; }
        public string Username { get; set; }

        // Editable properties
        public string Name { get; set; }
        public string Description { get; set; }
        public string Format { get; set; }

        public int DeckSignatureCardMultiverseid { get; set; }

        // Cards
        public Dictionary<int, int> DeckCardReferences { get; set; }
        public Dictionary<int, int> SideboardCardReferences { get; set; }
        public Dictionary<int, int> MaybeboardReferences { get; set; }

        [BsonIgnore]
        public Card DeckSignatureCard
        {
            get
            {
                return new CardRepository().GetCardByMultiverseId(DeckSignatureCardMultiverseid);
            }
        }
    }
}
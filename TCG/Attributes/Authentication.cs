﻿using System;
using System.Web;
using System.Web.Mvc;
using TCG.Managers;

namespace TCG.Attributes
{
    [AttributeUsage(AttributeTargets.All)]
    public class Authentication : ActionFilterAttribute
    {
        private bool _Enabled = true;

        public bool Enabled { get { return _Enabled; } set { _Enabled = value; } }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);

            // If not enabled, do nothing
            if (!Enabled) { return; }

            // If there is no Current User, redirect to Login Page
            if (SessionManager.Current.User == null)
            {
                var controller = filterContext.ActionDescriptor.ControllerDescriptor.ControllerName;
                var action = filterContext.ActionDescriptor.ActionName;
                var parameters = filterContext.HttpContext.Request.QueryString.ToString();

                filterContext.Controller.TempData.Add("controller", controller);
                filterContext.Controller.TempData.Add("action", action);
                filterContext.Controller.TempData.Add("parameters", parameters);

                filterContext.Result = new RedirectResult("/Account/Login");
            }
        }
    }
}
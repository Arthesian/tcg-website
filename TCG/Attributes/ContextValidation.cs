﻿using System;
using System.Web.Mvc;
using TCG.Managers;

namespace TCG.Attributes
{
    [AttributeUsage(AttributeTargets.All)]
    public class ContextValidation : ActionFilterAttribute
    {
        private bool _Enabled = true;
        public bool Enabled { get { return _Enabled; } set { _Enabled = value; } }

        private bool _UserHasDeck = true;
        public bool UserHasDeck { get { return _UserHasDeck; } set { _UserHasDeck = value; } }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);

            // If not Enabled, do nothing
            if (!_Enabled) { return; }

            // If some context needs to be checked : for example : Deck, Card or Game objects need to be loaded or something
            if(_UserHasDeck)
            {
                filterContext.Controller.ViewBag.UserHasDeck = ValidateUserHasDeck();
            }
        }

        private bool ValidateUserHasDeck()
        {
            return false;
        }
    }
}
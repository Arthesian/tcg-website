﻿var TCG = TCG || {};

TCG.charts = (function () {

    var _ = {};

    _.colors = { W: "rgb(255, 255, 225)", U: "rgb(160, 221, 249)", B: "rgb(196, 184, 182)", R: "rgb(246, 162, 139)", G: "rgb(140, 211, 170)", C: 'Gray' };
    _.titleContainer;
    _.chartContainer;

    var generic_options;

    var init = function () {
        var baseContainer = $('<div>').attr('id', 'chartsContainer').addClass('container');
        var baseRow = $('<div>').addClass('row');

        var titleContainer = $('<div>').addClass('col-12');
        _.titleContainer = titleContainer;

        var subContainer = $('<div>').addClass('chartsContainer-inner col-12');
        var subRow = $('<div>').addClass('row');

        subContainer.append(subRow);

        baseRow.append(titleContainer);
        baseRow.append(subContainer);

        baseContainer.append(baseRow);

        $(document.body).append(baseContainer);

        _.chartContainer = subRow;

        generic_options = {
            tooltip: {
                trigger: 'axis',
                axisPointer: {
                    type: 'shadow'
                }
            },
            grid: {
                left: '10px',
                right: '10px',
                bottom: '10px',
                containLabel: true
            },
            yAxis: [
                {
                    type: 'value'
                }
            ],
        };

        // Risize charts on window resize
        window.onresize = function () {
            $(".graphContainer").each(function () {
                var id = $(this).attr('_echarts_instance_');
                window.echarts.getInstanceById(id).resize();
            });
        };
    }

    _.openGraphsForCards = function (titleText, cards) {

        // Create dummy deck with supplied cards
        var deck = new TCG.deck.create();
        deck.addCards(cards);

        // Set title
        var title = $('<h2>').html(titleText);
        _.titleContainer.html(title);

        // Clear base pop-up container
        _.chartContainer.empty();

        // Get graph container
        var cmcGraphContainer = $('<div>').addClass('graphContainer col-12 col-lg-6');
        var colorGraphContainer = $('<div>').addClass('graphContainer col-12 col-lg-6');

        // Append the graph container to the main container
        _.chartContainer.append(cmcGraphContainer);
        _.chartContainer.append(colorGraphContainer);

        // Get data for the chart
        var cmcData = deck.analyser.getConvertedManaDistribution();
        var colorData = deck.analyser.getCardColorDistribution();

        $.fancybox.open({
            src: $('#chartsContainer'),
            type: 'inline',
            opts: {
                afterShow: (instance, current) => {
                    // Load the specific chart into the container
                    TCG.charts.setCMCDistributionGraph(cmcData, cmcGraphContainer[0]);
                    TCG.charts.setColorDistributionGraph(colorData, colorGraphContainer[0]);
                }
            }
        });
    }

    _.openGraphsForDeck = function (deck) {

        // Set title
        var title = $('<h2>').html('Deck Statistics');
        _.titleContainer.html(title);

        // Clear base pop-up container
        _.chartContainer.empty();

        // Get graph container
        var cmcGraphContainer = $('<div>').addClass('graphContainer col-12 col-lg-6');
        var colorGraphContainer = $('<div>').addClass('graphContainer col-12 col-lg-6');
        var typeGraphContainer = $('<div>').addClass('graphContainer col-12 col-lg-6');
        var rarityGraphContainer = $('<div>').addClass('graphContainer col-12 col-lg-6');

        // Append the graph container to the main container
        _.chartContainer.append(cmcGraphContainer);
        _.chartContainer.append(colorGraphContainer);
        _.chartContainer.append(typeGraphContainer);
        _.chartContainer.append(rarityGraphContainer);

        // Get data for the chart
        var cmcData = deck.analyser.getConvertedManaDistribution();
        var colorData = deck.analyser.getCardColorDistribution();
        var typeData = deck.analyser.getCardTypeDistribution();
        var rarityData = deck.analyser.getRarityDistribution();

        $.fancybox.open({
            src: $('#chartsContainer'),
            type: 'inline',
            opts: {
                afterShow: (instance, current) => {
                    // Load the specific chart into the container
                    TCG.charts.setCMCDistributionGraph(cmcData, cmcGraphContainer[0]);
                    TCG.charts.setColorDistributionGraph(colorData, colorGraphContainer[0]);
                    TCG.charts.setCardTypeDistributionGraph(typeData, typeGraphContainer[0]);
                    TCG.charts.setRarityDistributionGraph(rarityData, rarityGraphContainer[0]);
                }
            }
        });
    }

    _.setCMCDistributionGraph = function (data, container) {
        var xAxis = Object.keys(data);
        var values = Object.values(data);

        var options = {
            color: ['#3398DB'],
            title: {
                text: 'Converted Mana Distribution'
            },
            xAxis: [
                {
                    type: 'category',
                    data: xAxis,
                    axisTick: {
                        alignWithLabel: true
                    }
                }
            ],
            series: [
                {
                    name: 'Cards',
                    type: 'bar',
                    barWidth: '60%',
                    data: values
                }
            ]
        }

        var chart_options = Object.assign({}, generic_options, options);

        if (container) {
            var chart = echarts.init(container);
            chart.setOption(chart_options);
        } else {
            console.log('No container was specified to put the chart in!');
        }
    }

    _.setCardTypeDistributionGraph = function (origData, container) {

        var keys = Object.keys(origData);

        var data = [];

        keys.forEach((k, i) => {
            data.push({ name: k, value: origData[k].count });
        });

        var options = {
            title: {
                text: 'Card Type Distribution'
            },
            tooltip: {
                trigger: 'item',
                formatter: "{a} <br/>{b} : {c} ({d}%)"
            },
            legend: {
                orient: 'vertical',
                left: 'right',
                data: keys
            },
            series: [
                {
                    name: 'Cards',
                    center: ['33%', '50%'],
                    type: 'pie',
                    data: data
                }
            ]
        }

        var chart_options = Object.assign({}, options);

        if (container) {
            var chart = echarts.init(container);
            chart.setOption(chart_options);
        } else {
            console.log('No container was specified to put the chart in!');
        }
    }

    _.setColorDistributionGraph = function (data, container) {

        var colors = _.colors;

        var keys = Object.keys(data);
        var values = Object.values(data);

        var data = [];

        keys.forEach((k, i) => {
            var obj = { name: k, value: values[i] };
            if (colors[k]) {
                obj.itemStyle = {};
                obj.itemStyle.color = colors[k]
            }
            data.push(obj);
        });

        var options = {
            title: {
                text: 'Color Distribution'
            },
            tooltip: {
                trigger: 'item',
                formatter: "{a} <br/>{b} : {c} ({d}%)"
            },
            legend: {
                orient: 'vertical',
                left: 'right',
                data: keys
            },
            series: [
                {
                    name: 'Cards',
                    center: ['33%', '50%'],
                    type: 'pie',
                    data: data
                }
            ]
        }

        var chart_options = Object.assign({}, options);

        if (container) {
            var chart = echarts.init(container);
            chart.setOption(chart_options);
        } else {
            console.log('No container was specified to put the chart in!');
        }
    }

    _.setRarityDistributionGraph = function (data, container) {
        var colors = { rare: "Gold", 'mythic': "darkorange", uncommon: "Silver", common: "White", special: "Purple", 'basic land': 'Brown' }

        var keys = Object.keys(data);
        var values = Object.values(data);

        var data = [];

        var orderedData = [];
        var orderedKeys = [];
        var theOrder = ["basic land", "common", "uncommon", "rare", "mythic", "special"];

        keys.forEach((k, i) => {
            var obj = { name: k, value: values[i] };
            if (colors[k]) {
                obj.itemStyle = {};
                obj.itemStyle.color = colors[k]
            }
            data.push(obj);
        });

        for (var i = 0; i < theOrder.length; i++) {
            var currentKey = theOrder[i];

            data.forEach(d => {
                if (d.name == currentKey) {
                    orderedKeys.push(currentKey);
                    orderedData.push(d);
                }
            });
        }

        var options = {
            title: {
                text: 'Card Rarity Distribution'
            },
            xAxis: [
                {
                    type: 'category',
                    data: orderedKeys,
                    axisTick: {
                        alignWithLabel: true
                    }
                }
            ],
            series: [
                {
                    name: 'Cards',
                    type: 'bar',
                    barWidth: '60%',
                    data: orderedData
                }
            ]
        }

        var chart_options = Object.assign({}, generic_options, options);

        if (container) {
            var chart = echarts.init(container);
            chart.setOption(chart_options);
        } else {
            console.log('No container was specified to put the chart in!');
        }
    }

    _.setDevotionDistributionGraph = function (data, container) {
        var colors = _.colors;

        var keys = Object.keys(data);
        var values = Object.values(data);

        var data = [];

        keys.forEach((k, i) => {
            var obj = { name: k, value: values[i] };
            if (colors[k]) {
                obj.itemStyle = {};
                obj.itemStyle.color = colors[k]
            }
            data.push(obj);
        });

        var options = {
            title: {
                text: 'Devotion Distribution'
            },
            tooltip: {
                trigger: 'item',
                formatter: "{a} <br/>{b} : {c} ({d}%)"
            },
            legend: {
                orient: 'vertical',
                left: 'right',
                data: keys
            },
            series: [
                {
                    name: 'Cards',
                    center: ['33%', '50%'],
                    type: 'pie',
                    data: data
                }
            ]
        }

        var chart_options = Object.assign({}, options);

        if (container) {
            var chart = echarts.init(container);
            chart.setOption(chart_options);
        } else {
            console.log('No container was specified to put the chart in!');
        }
    }

    _.setLandInHandChanceDistribution = function (data, container) {
        var colors = _.colors;

        var keys = Object.keys(data);
        var values = Object.values(data);

        var data = [];

        keys.forEach((k, i) => {
            var obj = { name: k, value: values[i] };
            if (colors[k]) {
                obj.itemStyle = {};
                obj.itemStyle.color = colors[k]
            }
            data.push(obj);
        });

        var options = {
            title: {
                text: 'Land Draw Chances'
            },
            tooltip: {
                trigger: 'item',
                formatter: "{a} <br/>{b} : {c} ({d}%)"
            },
            legend: {
                orient: 'vertical',
                left: 'right',
                data: keys
            },
            series: [
                {
                    name: 'Lands',
                    center: ['33%', '50%'],
                    type: 'pie',
                    data: data
                }
            ]
        }

        var chart_options = Object.assign({}, options);

        if (container) {
            var chart = echarts.init(container);
            chart.setOption(chart_options);
        } else {
            console.log('No container was specified to put the chart in!');
        }
    }

    _.setLandManaGeneratingColorDistribution = function (data, container) {
        var colors = _.colors;

        var keys = Object.keys(data);
        var values = Object.values(data);

        var data = [];

        keys.forEach((k, i) => {
            var obj = { name: k, value: values[i] };
            if (colors[k]) {
                obj.itemStyle = {};
                obj.itemStyle.color = colors[k]
            }
            data.push(obj);
        });

        var options = {
            title: {
                text: 'Land Mana Color Distribution'
            },
            tooltip: {
                trigger: 'item',
                formatter: "{a} <br/>{b} : {c} ({d}%)"
            },
            legend: {
                orient: 'vertical',
                left: 'right',
                data: keys
            },
            series: [
                {
                    name: 'Mana',
                    center: ['33%', '50%'],
                    type: 'pie',
                    data: data
                }
            ]
        }

        var chart_options = Object.assign({}, options);

        if (container) {
            var chart = echarts.init(container);
            chart.setOption(chart_options);
        } else {
            console.log('No container was specified to put the chart in!');
        }
    }

    init();

    return _;

})();
﻿TCG = TCG || {};
TCG.templater = TCG.templater || {};

/**
 * Create default deck tile
 * 
 * @param {any} deck
 */
TCG.templater.deck = function (deck) {

    // Create the deck specific content
    var deckContent = $('<div>');

    var signatureCardImage = $('<img>').attr('src', TCG.helper.getCardImageUrlFromMultiverseid(deck.signatureCardId));
    deckContent.append(deckImage);

    var deckImage = $('<img>').attr('src', '/Static/Images/card-back.jpg');

    deckContent.append(deckImage);
    deckContent.append(deckImage);

    // Get the base object template
    var baseObj = TCG.templater.getBaseObject('deck');

    // Merge the content with the template
    baseObj.title.val(deck.name);
    baseObj.body.append(deckContent);
    baseObj.footer.val(deck.format);

    // Add data object to template
    baseObj.template.data('deck', deck);

    // Return the base template
    return baseObj.template;
}
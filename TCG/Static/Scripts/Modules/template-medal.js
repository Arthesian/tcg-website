﻿TCG = TCG || {};
TCG.templater = TCG.templater || {};

/**
 * Create default medail tile ( Achievement )
 * 
 * @param {any} medal
 */
TCG.templater.medal = function (medal) {

    // Create the custom content for a medal
    var medalContent = $('<div>');
    var medalImage = $('<img>').attr('src', medal.image_url);

    medalContent.append(medalImage);

    // Get the base template object
    var baseObj = TCG.templater.getBaseObject('medal');

    // Apply content to the template
    baseObj.title.val(medal.title);
    baseObj.body.append(medalContent);

    // Delete footer as it is not used for medals
    delete baseObj.footer;

    // Add data object to template
    baseObj.template.data('medal', medal);

    // Return the template object
    return baseObj.template;
}
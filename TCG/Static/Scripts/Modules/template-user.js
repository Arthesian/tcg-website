﻿TCG = TCG || {};
TCG.templater = TCG.templater || {};

/**
 * Create default User tile
 * 
 * @param {any} user
 */
TCG.templater.user = function (user) {

    // Profile picture box
    var profileImage = $('<img>').attr('src', user.ProfileImageUrl).css('width', '100%').addClass('user-tile-image');

    // User status box
    var userStatus = $('<div>').addClass('status status--ball user-tile-status').attr('data-user-status', user.Status);

    // Retrieve the base template object   
    var baseObj = TCG.templater.getBaseObject('user-tile');

    // Apply content to the template
    baseObj.title.html(user.Username);
    baseObj.body.append(profileImage, userStatus);
    baseObj.footer.remove();

    baseObj.template.addClass('col-4 col-sm-3 col-lg-2 mb-3').attr('href', '/User/Name/?name=' + user.Username);

    // Add data object to template
    baseObj.template.data('user', user);

    // Return the template for a user tile
    return baseObj.template;
}
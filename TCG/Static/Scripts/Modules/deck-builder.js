﻿TCG = TCG || {};
TCG.deck = TCG.deck || {};

TCG.deck.builder = function (mainboardElement, sideboardElement, maybeboardElement) {

    var __dragula;

    var _ = {
        deck: new TCG.deck.create(),
        useCardTypeColumns: true
    };

    var init = function () {
        initSearch();

        initDragula();

        $('#btn-deck-stats').on('click', () => { TCG.charts.openGraphsForDeck(_.deck); });
    }

    var initSearch = function () {
        var simpleCardSearch = TCG.helper.simpleSearch('/Card/Search', $("#card-search"), TCG.templater.card, $('.card-results'));
        simpleCardSearch.limit = 4;
    }

    var initDragula = function () {
        __dragula = dragula({
            isContainer: (el) => {
                return el.classList.contains('d-container') || el.classList.contains('card-drop-box')
            },
            copy: (el, source) => { return source.classList.contains("card-results"); },
            removeOnSpill: true,
            moves: (el, src, handle, sibling) => {
                return el.classList.contains('mtg-card-container');
            },
            accepts: (el, target, source) => {
                return source.id != target.id && !target.classList.contains("card-results");
            }
        }).on('drop', (el, container, sourceContainer, sibling) => {

            if (container.id == 'deck-mainboard-carddrop') {
                var card = $(el).find('article').data('card');

                _.deck.addCard(card);

                // Clear card drop boxes
                $('.card-drop-box').empty();

                redrawBoards();
            }
        }).on('drag', (el) => {
            $('.card-drop-box').removeClass('hidden');
        }).on('dragend', (el) => {
            $('.card-drop-box').addClass('hidden');
        }).on('cloned', (clone, original) => {
                var card = $(original).find('article').data('card');
                $(clone).find('article').data('card', card);
            }
        ).on('over', (el, container, source) => {

                // Add active class to receiving container
                $(container).addClass('active')
            }
        ).on('out', (el, container, source) => {

                // Remove active class from receiving container
                $(container).removeClass('active')
            }
        );
    }

    var redrawBoards = function () {

        if (_.deck.cards.length) {
            $('#btn-deck-stats').removeClass('hidden')
        } else {
            $('#btn-deck-stats').addClass('hidden');
        }

        var distribution = _.deck.analyser.getCardTypeDistribution();

        $('#deck-mainboard').empty();

        Object.keys(distribution).forEach(key => {
            var stackTemplate = getStackHtml(key, distribution[key].cards);

            $('#deck-mainboard').append(stackTemplate);
        });
    }

    var getStackHtml = function(titleText, cards) {

        var baseContainer = $('<div>').addClass('col-12');

        var container = $('<div>').addClass('cardstack card mb-4 shadow-sm');

        // Open charts?
        var chartIcon = $('<i>').addClass('fas fa-chart-pie')
        var chartButton = $('<button>').html(chartIcon).addClass('btn btn-sm btn-info').css('float', 'right').on('click', () => {
            TCG.charts.openGraphsForCards(titleText, cards);
        });

        var header = $('<div>').addClass('card-header');
        var headerText = $('<h4>').addClass('mb-0').html(titleText + ` (${cards.length}x)`);

        header.append(chartButton);
        header.append(headerText);

        container.append(header);

        var containerInner = $('<div>').addClass('cardstack-inner card-body');

        var cardContainer = $('<div>').addClass('cardstack-cards');

        cards.forEach(c => {
            var cardTemplate = TCG.templater.card(c, 'deckcard');

            cardTemplate.find('.mtg-card-header');

            cardContainer.append(cardTemplate);
        });

        containerInner.append(cardContainer);

        container.append(containerInner);

        var clearfix = $('<div>').addClass('clearfix');

        container.append(clearfix);

        baseContainer.append(container);

        return baseContainer;
    }

    _.setDeck = function (deck) {
        _.deck = deck;

        _.redrawBoards();
    }

    init();

    return _;

};
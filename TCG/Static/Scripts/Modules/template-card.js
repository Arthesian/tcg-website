﻿TCG = TCG || {};
TCG.templater = TCG.templater || {};

/**
 * Create default card tile
 * 
 * @param {any} card
 */
TCG.templater.card = function (card, fancyboxGroup, containerClasses) {

    // Get card specific content
    var cardContent = $('<div>').addClass('mtg-card--perspective');

    var cardImage = $('<img>').attr('src', TCG.helper.getCardImageUrlFromMultiverseid(card.multiverseid)).addClass('mtg-card');

    cardContent.append(cardImage);

    // Get the base template object
    var baseObj = TCG.templater.getBaseObject('mtg-card');

    // Merge the content for card with base template
    baseObj.title.html(card.name);
    baseObj.body.append(cardContent);
    baseObj.footer.remove();

    if (containerClasses) {
        baseObj.template.addClass(containerClasses);
    }

    baseObj.template.attr('href', TCG.helper.getCardImageUrlFromMultiverseid(card.multiverseid, 'large')).attr('data-fancybox', fancyboxGroup);

    // Add data object to template
    baseObj.container.data('card', card);
    baseObj.container.attr('data-id', card.Id);

    // Return template
    return baseObj.template;
}
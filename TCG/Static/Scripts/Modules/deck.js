﻿TCG = TCG || {};
TCG.deck = TCG.deck || {};

TCG.deck.create = function (data) {

    // Properties in scope
    var _ = {
        analyser: null,
        cardsData: {},

        // Define getter for cards
        get cards() { return Object.values(this.cardsData); }
    };

    // initialization function
    var init = function (cardData) {
        _.cardsData = cardData || {};
        _.analyser = new TCG.deck.analyser(_);

        reloadAnalyzer();
    };

    // Add multiple cards to deck
    _.addCards = function (cards) {

        // Loop over array, add all cards
        cards.forEach((c) => {
            _.addCard(c, true);
        });

        reloadAnalyzer();
    };

    // Add single card to deck
    _.addCard = function (card, skipReload) {

        var id = card.multiverseid;

        // If the card is already in the deck, up the amount. Else add it with amount = 1.
        if (_.cardsData[id]) {
            _.cardsData[id].amount++;
        } else {
            card.amount = 1;
            _.cardsData[id] = card;
        }

        // Optional skip reload analyzer
        if (!skipReload) {
            reloadAnalyzer();
        }
    };

    // remove a card from the deck
    _.removeCard = function (card) {

        var id = card.multiverseid;

        // If the card is in the deck
        if (_.cardsData[id]) {

            // Lower the number of cards
            _.cardsData[id].amount--;

            // If the amount is 0...remove it from the deck
            if (!_.cardsData[id].amount) {
                delete _.cardsData[id]
            }
        }

        // Reload the deck analyzer
        reloadAnalyzer();
    };

    // Reload the analyzer based on the current deck
    var reloadAnalyzer = function () {
        _.analyser.reload();
    };

    // initialize the deck with the JSON data set
    init(data);

    // Return the scope object
    return _;
};
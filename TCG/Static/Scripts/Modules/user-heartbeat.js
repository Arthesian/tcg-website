﻿var TCG = TCG || {};
TCG.user = TCG.user || {};

/**
 * Heartbeat module for the user that is related to 
 * the user's online activity. General rule :
 * - Every 'Alive()' call, keeps the user 'Online' for X minutes
 * - Every 'HeartBeat()' interval call, will keep the user 'inactve'
 *      when the 'Alive()' call hasn't been made after some time
 * - If no 'HeartBeat()' is sent after X seconds, the user is considered offline
 */
TCG.user.heartbeat = (function ($) {

    var _ = {};

    _.userId;

    var init = function () {
        _.alive();
    }

    _.setUserId = function (id) {
        _.userId = id;

        _.getUserStatus(id);
    }

    /**
     * Heartbeat pings server on each page load or when it's called
     * by page code. This signifies that the user is actively using
     * the site ( instead of the heartbeatInterval )
     */
    _.alive = function () {

        if (!TCG.user.isAuthenticated) { return; }

        $.ajax({
            url: "/user/heartbeat",
            type: "POST",
            data: {
                real: true
            }
        });
    }

    /**
     * HeartbeatInterval pings server every couple of seconds
     * to update the user's last-active timestamp. Is 
     * used to record the user's status.
     */
    var heartbeatInterval = function () {

        setInterval(function () {

            if (_.userId) {
                _.getUserStatus(_.userId);
            }

            if (!TCG.user.isAuthenticated) { return; }

            $.ajax("/user/heartbeat");

        }, 5000);

    };

    _.getUserStatus = function (id) {

        if (id) {

            $.post("/User/GetBaseJsonUserById", { id: id }).done(function (suser) {

                var user = JSON.parse(suser);

                if (user) {
                    var node = document.querySelectorAll("[data-user=" + user.Username + "].status");

                    var status = user.Status.toLowerCase();
                    var dateString = new Date(user.LastActivity).toLocaleString();

                    if (node) {
                        node.forEach(function (n) {
                            n.setAttribute("data-user-status", user.Status);
                            n.setAttribute("title", status + "; last activity at " + dateString)
                        });
                    }
                }
            });

        } else {
            console.warn("No user ID was supplied to 'getUserStatus(id)'!"); return;
        }

    };

    // Initialization
    init();

    return _;

})(jQuery);
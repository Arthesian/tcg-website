﻿TCG = TCG || {};
TCG.templater = TCG.templater || {};

/**
 * Get base template object for item tiles
 * 
 * @param {any} Cssclass
 */
TCG.templater.getBaseObject = function (Cssclass) {

    // Set default classname for elements if none is passed
    Cssclass = Cssclass || 'template';

    var link = $('<a>').addClass(Cssclass + '-container');

    var container = $('<article>').addClass(Cssclass + '-article');

    var header = $('<div>').addClass(Cssclass + '-header');
    var title = $('<h2>').addClass(Cssclass + '-title');

    // Combine header
    header.append(title);

    var body = $('<div>').addClass(Cssclass + '-body');
    var footer = $('<div>').addClass(Cssclass + '-footer');

    // Combine all the elements
    container.append(header);
    container.append(body);
    container.append(footer);

    link.append(container);

    // Return the base template object
    return {
        title: title,
        body: body,
        footer: footer,
        container: container,
        template: link
    }
}
﻿using MongoDB.Driver.Linq;
using System.Linq;
using TCG.Entities;

namespace TCG.Helpers
{
    public static class DeckQueryExtensions
    {
        public static IMongoQueryable<Deck> ByName(this IMongoQueryable<Deck> query, string name)
        {
            name = name.ToLower();
            return query.Where(x => x.Name.ToLower().Contains(name));
        }

        public static IMongoQueryable<Deck> ByUser(this IMongoQueryable<Deck> query, User user)
        {
            return query.ByUserName(user.Username);
        }

        public static IMongoQueryable<Deck> ByUserName(this IMongoQueryable<Deck> query, string username)
        {
            username = username.ToLower();
            return query.Where(x => x.Username.ToLower().Contains(username));
        }

        public static IMongoQueryable<Deck> ByQuery(this IMongoQueryable<Deck> query, string searchText)
        {
            searchText = searchText.ToLower();
            return query.Where(x => x.Username.ToLower().Contains(searchText) || x.Description.ToLower().Contains(searchText) || x.Name.ToLower().Contains(searchText));
        }
    }
}
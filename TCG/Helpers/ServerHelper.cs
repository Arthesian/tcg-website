﻿using System.Web.Hosting;

namespace TCG.Helpers
{
    public static class ServerHelper
    {
        public static string RelativePath(string path)
        {
            return path.Replace(HostingEnvironment.ApplicationPhysicalPath, "~/").Replace(@"\", "/");
        }
    }
}
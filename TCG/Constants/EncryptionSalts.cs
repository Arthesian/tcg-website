﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TCG.Constants
{
    public class EncryptionSalts
    {
        // generated encryption salt
        public const string PasswordSalt = "MQZ%E4vtZ%d6VE5po1asDFGgsfF84$1H^&SD32@$DSFG23dd3SIlSg%F9ZIu9kW!cQF0Q@IRnc*fxfXoa";
    }
}
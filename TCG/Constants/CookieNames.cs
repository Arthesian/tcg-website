﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TCG.Constants
{
    public class CookieNames
    {
        public const string LoginRememberMe = "LoginRememberMe";
        public const string LoginUsername = "LoginUsername";
        public const string LoginPassword = "LoginPassword";
    }
}